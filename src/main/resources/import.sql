INSERT INTO account (username, email, password) VALUES('admin', 'admin@vanityweb.devbugger.com', '$2a$10$O1MMi3SLcvwtJIT9CSZyN.aLtFKN.K2LtKyHZ52wElo0zh5gI1EyW');
INSERT INTO account (username, email, password) VALUES('scav', 'scav@vanityweb.devbugger.com', '$2a$10$O1MMi3SLcvwtJIT9CSZyN.aLtFKN.K2LtKyHZ52wElo0zh5gI1EyW');

INSERT INTO category (title) VALUES('News');
INSERT INTO category (title) VALUES('Recruitment');
INSERT INTO category (title) VALUES('Dummy Category');
