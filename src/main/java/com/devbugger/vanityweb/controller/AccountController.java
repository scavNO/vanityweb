/*
 * Copyright 2014 Dag Østgulen Heradstveit
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devbugger.vanityweb.controller;

import com.devbugger.vanityweb.domain.Account;
import com.devbugger.vanityweb.service.AccountService;
import com.devbugger.vanityweb.exception.AccountNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Dag Østgulen Heradstveit
 */
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Account> all() {
        return accountService.findAll();
    }

    @RequestMapping(value = "/{username}", method = RequestMethod.GET)
    public Account byUsername(@PathVariable("username") String username) {
        return accountService.findByUsername(username).orElseThrow(
                () -> new AccountNotFoundException(username)
        );
    }
}