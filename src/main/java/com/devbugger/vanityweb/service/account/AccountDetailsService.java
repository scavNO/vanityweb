/*
 * Copyright 2014 Dag Østgulen Heradstveit
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devbugger.vanityweb.service.account;

import com.devbugger.vanityweb.domain.Account;
import com.devbugger.vanityweb.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AccountDetailsService  implements UserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(AccountDetailsService.class);

    private AccountService accountService;

    @Autowired
    public AccountDetailsService(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.info("Trying to log in " + username);

        Account account = accountService.findByUsername(username).orElseThrow(
                () -> new UsernameNotFoundException(username)
        );

        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();

        logger.info("Assigning authorities to " + username);
        account.getAuthorities().forEach((authority) -> {
            grantedAuthorities.add(new SimpleGrantedAuthority(authority.getAuthority()));
            logger.info(authority.getAuthority());
        });

        return new AccountDetails(account, grantedAuthorities);
    }
}