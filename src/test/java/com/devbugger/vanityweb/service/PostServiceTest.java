package com.devbugger.vanityweb.service;

import com.devbugger.vanityweb.VanityWebApplication;
import com.devbugger.vanityweb.domain.Post;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = VanityWebApplication.class)
@WebAppConfiguration
@Transactional
public class PostServiceTest {

    @Autowired
    private PostService postService;

    @Autowired
    private CategoryService categoryService;

    @Test
    public void testSavePost() throws Exception {
        Post post = new Post();
        post.setTitle("A title");
        post.setContent("This content is all about the importance of writing tests while at " +
                        "the same time accepting that you should not over analyze it.");
        post.setDateTimePosted(LocalDateTime.now());

        post.setCategory(categoryService.findById(1L));

        post = postService.save(post);

        System.out.println(post.getCategory());
    }
}