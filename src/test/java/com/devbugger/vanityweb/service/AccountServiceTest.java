/*
 * Copyright 2014 Dag Østgulen Heradstveit
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devbugger.vanityweb.service;

import com.devbugger.vanityweb.VanityWebApplication;
import com.devbugger.vanityweb.domain.Account;
import com.devbugger.vanityweb.exception.AccountNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = VanityWebApplication.class)
@WebAppConfiguration
public class AccountServiceTest {

    @Autowired
    private AccountService accountService;

    @Autowired
    private UserDetailsService userDetailsService;

    @Test
    public void test() throws Exception {
        Account a = accountService.findByUsername("admin").orElseThrow(
                () -> new AccountNotFoundException("admin")
        );

        System.out.println(a.toString());
    }

    @Test
    public void testUserDetails() throws Exception {
        UserDetails userDetails = userDetailsService.loadUserByUsername("scav");
        System.out.println(userDetails.getUsername());
        System.out.println(userDetails.getPassword());

        System.out.println(userDetails.toString());

    }
}
