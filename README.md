# VanityWeb Server
This is the server side of Vanity guild's web application.

It will be responsible for handling battle.net OAuth2 login and will act as a supplement to anything
which the battle.net API cannot provide.

## Running the application
Windows: gradlew.bat bootRun <br />
Linux ./gradlew bootRun


# Testing OAuth 2 with Curl
Login:<br />
curl -X POST -vu html5-vanityWeb:123456 http://localhost:8080/oauth/token -H "Accept: application/json"
-d "password=password&username=scav&grant_type=password&scope=write&client_secret=123456&client_id=html5-vanityWeb"
<br />
Fetch resource: <br />
curl -v GET http://localhost:8080/account/all -H "Authorization: Bearer af00d02d-2f40-4a61-8523-082cb2b8f3ff"